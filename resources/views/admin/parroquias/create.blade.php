@extends('admin.template.main')
@section('content')
   
    <h1> Crear Parroquia </h1>
	
 


	{!! Form::open(['route' => 'parroquias.store', 'method' => 'POST']) !!}

    <div class="form-group">
    	{!! Form::label('name','nombre') !!}
    	{!! Form::text('nombre',null,['class' => 'form-control', 'placeholder' => 'Nombre completo' , 'requiere']) !!}
     
    </div>

    <div>
    	{!! Form::submit('registrar', ['class' => 'btn btn-primary']) !!}
     
    </div>

	{!! Form::close() !!}
     
@endsection  

