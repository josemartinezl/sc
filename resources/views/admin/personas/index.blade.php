@extends('admin.template.main')
@section('title','Listado de Personas')

@section('content')
	
	<h1>Listado de Personas </h1>
		<a href="{{ route('personas.create') }}" class="btn btn-info"> Registar habitante</a>
			{!! Form::open(['route' => 'personas.index', 'method' => 'GET','class' => 'navbar-form pull-right']) !!}
				<div class="input-group">
					{!! Form::text('cedula',null,['class' => 'form-control','placeholder' => 'Buscar habitante por cedula', 'aria-describedby' => 'search']) !!}
					<span class="input-group-addon" id="search"> 
            		<span class="glyphicon glyphicon-search" aria-hidden="true">
            		</span>  
				</span>
			
				</div>
			
	{!! Form::close() !!}
		
		<table class="table table-striped">
		<thead>
	
			<th> Cedula </th>
			<th> Nombre </th>
			<th> Apellido </th>
			<th> Telefono </th>
		</thead>
		<tbody>
			@foreach($personas as $persona)
			<tr>
				<td>{{ $persona->cedula}} </td>
				<td>{{ $persona->nombre}} </td>
				<td>{{ $persona->apellido}} </td>
				<td>{{ $persona->telefono}} </td>
				<td> <a  class="btn btn-danger"></a>
					 <a  class="btn btn-warning"></a>
					 </td>
			</tr>
			@endforeach
		</tbody>
		
	</table>
	{!! $personas->render() !!}

@endsection