@extends('admin.template.main')
@section('title','Listado de Paises')

@section('content')
	
	<h1>Listado de Paises </h1>
	<a href="{{ route('paises.create') }}" class="btn btn-info"> Registar pais</a>
	{!! Form::open(['route' => 'paises.index', 'method' => 'GET','class' => 'navbar-form pull-right']) !!}
		<div class="input-group">
			{!! Form::text('nombre',null,['class' => 'form-control','placeholder' => 'Buscar pais...', 'aria-describedby' => 'search']) !!}
			<span class="input-group-addon" id="search"> 
            	<span class="glyphicon glyphicon-search" aria-hidden="true">
            	</span>  
			</span>
			
		</div>
			
	{!! Form::close() !!}
	<table class="table table-striped">
		<thead>
			<th> ID </th>
			<th> Nombre </th>
		</thead>
		<tbody>
			@foreach($paises as $pais)
			<tr>
				<td>{{ $pais->id}} </td>
				<td>{{ $pais->nombre}} </td>
				<td> <a href="{{ route('admin.paises.destroy', $pais->id) }}" onclick="return confirm('¿Seguro quiere eliminar el pais <?php echo $pais->nombre ?> ?' )" class="btn btn-danger"></a>
					 <a href="{{ route('paises.edit', $pais->id) }}" class="btn btn-warning"></a></td>
			</tr>
			@endforeach
		</tbody>
		
	</table>
	{!! $paises->render() !!}

@endsection