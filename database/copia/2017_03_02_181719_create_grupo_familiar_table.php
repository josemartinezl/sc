<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGrupoFamiliarTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('grupo_familiar', function(Blueprint $table)
		{
			$table->increments('ID')->primary();
			$table->string('nombre', 100);
			$table->string('direccion', 255);
			$table->integer('Vereda_ID')->index('fk_Grupo_Familiar_Vereda1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('grupo_familiar');
	}

}