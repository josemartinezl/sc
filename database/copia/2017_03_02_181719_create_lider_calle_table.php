<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLiderCalleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lider_calle', function(Blueprint $table)
		{
			$table->integer('Vereda_ID')->index('fk_Lider_calle_Vereda1_idx');
			$table->integer('Persona_ID')->index('fk_Lider_calle_Persona1_idx');
			$table->date('fecha');
			$table->integer('numero_familias');
			$table->primary(['Vereda_ID','Persona_ID']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lider_calle');
	}

}