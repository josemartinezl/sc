<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRegistroClapTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('registro_clap', function(Blueprint $table)
		{
			$table->foreign('Grupo_Familiar_ID', 'fk_Registro_clap_Grupo_Familiar1')->references('ID')->on('grupo_familiar')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('CLAP_ID', 'fk_table1_CLAP1')->references('ID')->on('clap')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('registro_clap', function(Blueprint $table)
		{
			$table->dropForeign('fk_Registro_clap_Grupo_Familiar1');
			$table->dropForeign('fk_table1_CLAP1');
		});
	}

}