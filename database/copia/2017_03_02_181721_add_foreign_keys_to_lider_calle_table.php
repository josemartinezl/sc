<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLiderCalleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('lider_calle', function(Blueprint $table)
		{
			$table->foreign('Persona_ID', 'fk_Lider_calle_Persona1')->references('ID')->on('persona')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('Vereda_ID', 'fk_Lider_calle_Vereda1')->references('ID')->on('vereda')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('lider_calle', function(Blueprint $table)
		{
			$table->dropForeign('fk_Lider_calle_Persona1');
			$table->dropForeign('fk_Lider_calle_Vereda1');
		});
	}

}