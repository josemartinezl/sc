<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePersonaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persona', function(Blueprint $table)
		{
			$table->increments('ID')->primary();
			$table->string('apellidos', 100);
			$table->string('nombres', 100);
			$table->string('parentesco', 100);
			$table->integer('Cedula')->nullable()->unique('Cedula_UNIQUE');
			$table->string('telefono', 45)->nullable();
			$table->date('fecha_nacimiento');
			$table->string('Estado_civil', 100);
			$table->integer('sexo');
			$table->string('enfermedad', 100)->nullable();
			$table->string('Nivel_de_instruccion', 50)->nullable();
			$table->string('ocupacion', 100)->nullable();
			$table->integer('Grupo_Familiar_ID')->index('fk_Persona_Grupo_Familiar1_idx');
			$table->string('lugar', 45);
			$table->integer('PAIS_ID')->index('fk_Persona_Pais1_idx');
			$table->integer('Jefe_familia')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('persona');
	}

}