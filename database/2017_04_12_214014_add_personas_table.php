<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',200);
            $table->string('apellido',200);
            $table->string('parentesco',200)->nullable();
            $table->integer('cedula')->unique()->nullable();
            $table->string('telefono',20)->nullable();
            $table->date('fecha_nacimiento');
            $table->string('estado_civil',200)->nullable();
            $table->integer('sexo');
            $table->string('enfermedad',200)->nullable();
            $table->string('nivel_instruccion',200)->nullable();
            $table->string('ocupacion',200)->nullable();
            $table->integer('grupo_familiar_id')->unsigned();
            $table->foreign('grupo_familiar_id')->references('id')->on('grupo_familiar')->onDelete('cascade');
            $table->string('lugar',200)->nullable();
            $table->integer('pais_id')->unsigned();
            $table->foreign('pais_id')->references('id')->on('pais')->onDelete('cascade');
            $table->string('jefe_familia',200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
