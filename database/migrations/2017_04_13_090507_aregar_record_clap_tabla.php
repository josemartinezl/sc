<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AregarRecordClapTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_clap', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clap_id')->unsigned();
            $table->foreign('clap_id')->references('id')->on('claps')->onDelete('cascade');
            $table->integer('grupo_familiar_id')->unsigned();
            $table->foreign('grupo_familiar_id')->references('id')->on('grupo_familiar')->onDeslete('cascade');
            $table->date('fecha')->nullable();
            $table->string('tipo_mercado',200)->nullable();
            $table->integer('entregado')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('record_clap');
    }
}
