<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGrupoFamiliarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupo_familiar', function (Blueprint $table) {
            $table->increments('id');
            $table->string(' nombre ',100)->unique('nombre_UNIQUE');
            $table->string('direccion',100);
            $table->integer('vereda_id')->unsigned();
            $table->foreign('vereda_id')->references('id')->on('vereda')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupo_familiar');
    }
}
