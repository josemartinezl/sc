<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegistroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_claps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('claps_id')->unsigned();
            $table->foreign('claps_id')->references('id')->on('claps')->onDelete('cascade');
            $table->integer('grupo_familiar_id')->unsigned();
            $table->foreign('grupo_familiar_id')->references('id')->on('grupo_familiar')->onDelete('cascade');
            $table->date('fecha')->nullable();
            $table->string('tipo_mercado',200)->nullable();
            $table->integer('entregado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registro_claps');
    }
}
