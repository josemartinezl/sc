<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSectorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sector', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',100)->unique('nombre_UNIQUE');
            $table->integer('parroquia_id')->unsigned();
            //si no quiero borrar en cascada le quito el onDelete('cascade');
            $table->foreign('parroquia_id')->references('id')->on('parroquia')->onDelete('cascade');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sector');
    }
}
