<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Grupo_familiar extends Model
{
	protected $table = "grupo_familiar";
	protected $fillable = ['nombre','direccion','vereda_id'];
    //
    
    public function personas(){
    	return $this->hasMany('App\Persona');
    }

    public function vereda(){

    	return $this->belongsTo('App\Vereda');
    }


    public function claps(){
	
	return $this->belongsToMany('App\Clap','record_clap')
			->using('App\Record_clap')
			->withPivot('fecha','tipo_mercado','entregado');
	}

}
