<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Http\Requests\PaisRequest;
use App\Persona;
use App\Pais;
use App\Grupo_familiar;

class PersonasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     
            $personas = Persona::Search($request->cedula)->orderBy('id','ASC')->paginate(2);
            return view('admin.personas.index')->with('personas',$personas);


        
       // return view('admin.personas.index');
        //
    }

  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$paises = Pais::orderBy('nombre','ASC')->get();
        $paises = Pais::select('nombre','id')->orderBy('nombre','ASC')->pluck('nombre','id');
     
        //dd($paises);
        $grupos = Grupo_familiar::select('nombre','id')->orderBy('nombre','ASC')->pluck('nombre','id');
        //return view('admin.personas.create')->with('paises',$paises)->with('grupos',$grupos);
        return view('admin.personas.create',['paises' => $paises , 'grupos' => $grupos]);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $personas = new Persona($request->all());

        //usamos el associate supuestamente para que no haya error al momneto de varias guardados a la vez
         $pais = Pais::find($personas->pais_id);
         $grupo = Grupo_familiar::find($personas->grupo_familiar_id);
         $personas->pais()->associate($pais);
         $personas->grupo_familiar()->associate($grupo);
         $personas->save();
        
        Flash::success(  $personas->nombre . " " . $personas->apellido . " se ha registrado");
        return redirect()->route('personas.index');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
    * @return \Illuminate\Http\Response
     */

    public function leader(){

         dd('hola mundo');
    }
}
