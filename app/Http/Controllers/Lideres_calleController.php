<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use App\Vereda;
use App\grupo_familiar;
use Illuminate\Support\Facades\DB;

class Lideres_calleController extends Controller
{
    
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         dd('esto es el index');
        //
    }

  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.lideres_calle.create'); 
    }

       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     	/*
     	me trae un collection
     	$persona = DB::table('personas')->where('cedula',$request->cedula)->get();
     	*/
     	
     	/*
     	me trae los valores directos
     	$persona = DB::table('personas')->where('cedula',$request->cedula)->first();
        */

        $personas = DB::table('personas')->where('cedula',$request->cedula)
     					->join('grupo_familiar','personas.grupo_familiar_id','=','grupo_familiar.id')
     					->join('vereda','grupo_familiar.vereda_id','=','vereda.id')
     					->select('personas.id as perid','vereda.id as verid')->get();
     

     	if($personas->isNotEmpty()){
             	
        	foreach ($personas as $persona) {
    			$person = Persona::find($persona->perid);
    			$vereda = Vereda::find($persona->verid);
                /* para guardar cuando la tabla pivote no tenga campos adicionales 
    			$person->veredas()->attach($vereda); 
                */

                // guardar cuando la clase pivote tenga campos adicionales
                $person->veredas()->attach($vereda,['numeros_familias' => $request->numeros_familias] );
    			
                // guardar cuando la clase pivote tenga campos adicionales
    			//$person->veredas()->save($vereda,['numeros_familias' => $request->numeros_familias] );
    			



    			dd('Persona guardada exitosamente');
			}     	
     	 
     	 //dd('Si existe el habitante');
		}else{
     		dd('no existe el habitante');

     	}
		}
   
}
