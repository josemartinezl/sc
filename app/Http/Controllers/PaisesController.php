<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pais;
use Laracasts\Flash\Flash;
use App\Http\Requests\PaisRequest;

class PaisesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paises = Pais::Search($request->nombre)->orderBy('id','ASC')->paginate(2);

        return view('admin.paises.index')->with('paises',$paises);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.paises.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaisRequest $request)
    {
        $pais = new Pais($request->all());
        $pais->save();

        Flash::success("Pais " . $pais->nombre . " se ha registrado");

        return redirect()->route('paises.index');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paises = Pais::find($id);
        return view('admin.paises.edit')->with('paises',$paises);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pais = Pais::find($id);
        $pais->nombre = $request->nombre;
        $pais->save();

        Flash::success('Pais ' . $pais->nombre . ' ha sido actualizada'); 

        return redirect()->route('paises.index');

        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pais = Pais::find($id);
        $pais->delete();
 
        Flash::error('Pais ' . $pais->nombre . ' ha sido borrado'); 

        return redirect()->route('paises.index');
        //
    }
}
