<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parroquia;
use Laracasts\Flash\Flash;
use App\Http\Requests\ParroquiaRequest;

class ParroquiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parroquias = Parroquia::orderBy('id','ASC')->paginate(2);

        return view('admin.parroquias.index')->with('parroquias',$parroquias);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.parroquias.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ParroquiaRequest $request)
    {
        //dd($request->all());
        $parroquia = new Parroquia($request->all());
        //dd($parroquia);
        $parroquia->save();

        Flash::success("Parroquia " . $parroquia->nombre . " se ha registrado");

        return redirect()->route('parroquias.index');

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parroquia = Parroquia::find($id);
        return view('admin.parroquias.edit')->with('parroquia',$parroquia);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $parroquia = Parroquia::find($id);
        $parroquia->nombre = $request->nombre;
        $parroquia->save();

        Flash::success('Parroquia ' . $parroquia->nombre . 'ha sido actualizada'); 

        return redirect()->route('parroquias.index');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      
        $parroquia = Parroquia::find($id);
        $parroquia->delete();

        Flash::error('Parroquia ' . $parroquia->nombre . 'ha sido borrada'); 

        return redirect()->route('parroquias.index');
        
        //
    }
}
