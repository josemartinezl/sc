<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot; 

class Persona extends Model
{
	protected $table = "personas";
	protected $fillable = ['nombre','apellido','parentesco','cedula','telefono','fecha_nacimiento','estado_civil','sexo',
						   'enfermedad','nivel_instruccion','ocupacion','grupo_familiar_id','lugar','pais_id','jefe_familia'];



	public function grupo_familiar(){
		return $this->belongsTo('App\Grupo_familiar');
	}


	public function pais(){
		return $this->belongsTo('App\Pais');
	}

	public function veredas(){
		return $this->belongsToMany('App\Vereda','lideres_calle')
			   ->using('App\Lider_calle')
			   ->withPivot('fecha_inicio','fecha_fin','numeros_familias')->withTimestamps();
	}


    public function scopeSearch($query, $cedula){

		//return $query->where('cedula',$cedula);
		return $query->where('cedula', 'LIKE',"%$cedula%");

	}
}
