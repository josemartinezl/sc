<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot; 

class Vereda extends Model
{
	protected $table = "vereda";
	protected $fillable = ['nombre','sector_id'];
    //

    public function sector(){

    	return $this->belongsTo('App\Sector');
    }

    public function grupo_familiares(){

    	return $this->hasMany('App\Grupo_Familiar');
    }

    public function personas(){
	
		return $this->belongsToMany('App\Persona','lideres_calle')
			   ->using('App\Lider_calle')
			   ->withPivot('fecha_inicio','fecha_fin','numeros_familias')->withTimestamps();
	}

}
