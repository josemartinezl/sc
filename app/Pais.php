<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
	protected $table = "pais";
	protected $fillable = ['nombre'];
    //


    public function personas(){
	
		return $this->hasMany('App\Persona');
	}

	public function scopeSearch($query, $nombre){

		return $query->where('nombre', 'LIKE',"%$nombre%");

	}
    
}
