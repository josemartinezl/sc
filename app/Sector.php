<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{

	protected $table = "sector";
	protected $fillable = ['nombre','parroquia_id'];

	public function parroquia(){
		return $this->belongsTo('App\Parroquia');
	}

	public function veredeas(){

		return $this->hasMany('App\Vereda');
	}
    //
}
