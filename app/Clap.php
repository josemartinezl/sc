<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Clap extends Model
{

	protected $table = "claps";
	protected $fillable = ['nombre'];


public function grupos(){
	
	return $this->belongsToMany('App\Grupo_familiar','record_clap')
			->using('App\Record_clap')
			->withPivot('fecha','tipo_mercado','entregado');
	}
    //
}
