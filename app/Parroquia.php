<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Parroquia extends Model
{

	use Sluggable;


	protected $table = "parroquia";
	protected $fillable = ['nombre'];

	public function sectores(){
		return $this ->hasMany('App\Sector');
	}

	 public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'nombre'
            ]
        ];
    }
    //
}
