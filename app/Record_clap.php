<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Record_clap extends Pivot
{

	protected $table = "record_clap";
	protected $fillable = ['claps_id','grupo_familiar_id','fecha','tipo_mercado','entregado'];
    //
}
