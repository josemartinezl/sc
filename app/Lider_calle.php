<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Lider_calle extends Pivot
{

	protected $table = "lideres_calle";
	protected $fillable = ['vereda_id','persona_id','fecha_inicio','fecha_fin','numeros_familias'];
    //
}
