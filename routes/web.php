<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});




Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){
	//rutas parroquias
	Route::resource('parroquias','ParroquiasController');
	Route::get('parroquias/{id}/destroy',[ 
		'uses' => 'ParroquiasController@destroy',
		'as' => 'admin.parroquias.destroy'	
		]);

	// ruta sector incompleta
	Route::resource('sectores','SectoresController');

	//rutas paises
	Route::resource('paises','PaisesController');
	Route::get('paises/{id}/destroy',[ 
		'uses' => 'PaisesController@destroy',
		'as' => 'admin.paises.destroy'	
		]);

	// rutas personas
	Route::resource('personas','PersonasController');
	/*
	Route::get('personas/leader',[ 
		'uses' => 'PersonasController@leader',
		'as' => 'admin.personas.leader'	
		]);*/
	// probar la otra forma de nombrar las rutas
	Route::get('/personas/leader', 'PersonasController@leader');

	//Route::get('/leader/leader', 'leader@crear');

	//rutas de lideres de calle

	Route::get('/lideres_calle', 'Lideres_calleController@index')->name('lideres_calle.index');
	Route::get('/lideres_calle/create', 'Lideres_calleController@create')->name('lideres_calle.create');
	Route::post('/lideres_calle', 'Lideres_calleController@store')->name('lideres_calle.store');
	//Route::get('/lideres_calle/leader', 'leader@');



});
Auth::routes();
Route::get('/home', 'HomeController@index');
